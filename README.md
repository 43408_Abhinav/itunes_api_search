# Getting Started with iTunes API Search

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


## Pre Requisite

To run the app the below packages need to be installed:
1. rect-dom 
2. react-scripts 
3. styled-components

### Command
```
npm install rect-dom react-scripts styled-components
```

## Available Scripts

In the project directory, you can run:

### `npm Create`

Creates the app.
```
npm create-react-app itunes_api_search
```

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.






